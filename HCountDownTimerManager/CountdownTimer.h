//
//  CountdownViewModel.h
//  SadjAppClient
//
//  Created by 送爱到家 on 2018/1/4.
//  Copyright © 2018年 huang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
//#import <ReactiveObjC.h>
@interface CountdownTimer : NSObject

@property(nonatomic) CGFloat interval;                       //时间间隔 默认为1S
@property(nonatomic) CGFloat allTimer;                       //总的时间 总时间
//@property(nonatomic,strong)RACScheduler *scheduler;         //设置线程 ，默认总线程
@property(nonatomic,strong) dispatch_queue_t queue;
//开始倒计时，从总时间开始
-(void)startRuning:(void (^)(CGFloat timer))block withTimerEnd:(dispatch_block_t)endBlock;
//比方总时间 60S 从 startImer 开始的时间倒计时
-(void)startRuningWihtStartTimer:(CGFloat)startTimer  withBlock:(void (^)(CGFloat timer))block withTimerEnd:(dispatch_block_t)endBlock;
-(void)stopRuning;

@end
